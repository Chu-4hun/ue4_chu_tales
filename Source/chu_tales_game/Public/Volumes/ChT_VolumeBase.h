// chu_game, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "ChT_VolumeBase.generated.h"

UCLASS()
class CHU_TALES_GAME_API AChT_VolumeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChT_VolumeBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "Trigger")
	UBoxComponent* Collider;

	UPROPERTY(BlueprintReadOnly, Category = "Trigger")
	bool isVisible;

	UFUNCTION(BlueprintImplementableEvent, Category = "Trigger" )
	void OnOverlap();
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
