// chu_game, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "ChT_ComboClass.generated.h"

/**
 * 
 */
UCLASS()
class CHU_TALES_GAME_API UChT_ComboClass : public UDataAsset
{
	GENERATED_BODY()
public:
	/**
	 * @brief Array of combo animation montages
	 *Массив анимаций для комбо
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Combo")
	TArray<UAnimMontage*> ComboAnimations;

	/**
	 * @brief Delay between combo animations
	 * Задержка между анимациями комбо
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="Combo")
	float ComboDelay = 0;

	/**
	 * @brief 
	 * @return Return next combo animation
	 * Возвращает следующую анимацию комбо
	 */


	
	UChT_ComboClass();
	~UChT_ComboClass();

	
};
