// chu_game, All Rights Reserved


#include "ChT_CharacterBase.h"

#include "Components/ChT_CharacterMovementComponent.h"
#include "Components/ChT_HealthComponent.h"
#include "Components/ChT_WeaponComponent.h"


DEFINE_LOG_CATEGORY_STATIC(CharacterBaseLog, All, All);
// Sets default values
/**
 * @brief Конструктор класса
 * @param ObjInit 
 */
AChT_CharacterBase::AChT_CharacterBase(const FObjectInitializer& ObjInit): Super(
	ObjInit.SetDefaultSubobjectClass<UChT_CharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UChT_HealthComponent>("HealthComponent");
	
	WeaponComponent = CreateDefaultSubobject<UChT_WeaponComponent>("Weapon Component");
}

// Called when the game starts or when spawned
/**
 * @brief Начало игры
 */
void AChT_CharacterBase::BeginPlay()
{
	Super::BeginPlay();

	OnHealthChanged(HealthComponent->GetHealth());
}

/**
 * @brief Эвент по проигрышу
 */
void AChT_CharacterBase::OnDeath()
{
}

/**
 * @brief Эвент по изменению здоровья
 * @param Health количетсво жизни
 */
void AChT_CharacterBase::OnHealthChanged(float Health)
{
}

// Called every frame
/**
 * @brief Эвент на каждый кадр
 * @param DeltaTime дельта времени
 */
void AChT_CharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/**
 * @brief Передвижение вперед
 * @param Amount количество 
 */
void AChT_CharacterBase::MoveForward(float Amount)
{
	IsMovingForward = Amount > 0.0f;
	AddMovementInput(GetActorForwardVector(), Amount);
}

/**
 * @brief Передвижение вправо
 * @param Amount 
 */
void AChT_CharacterBase::MoveRight(float Amount)
{
	if (Amount == 0.0f) return;
	AddMovementInput(GetActorRightVector(), Amount);
}

/**
 * @brief Эвент по началу бега
 */
void AChT_CharacterBase::OnStartRunning()
{
	WantsToRun = true;
}

/**
 * @brief  Эвент по окончанию бега
 */
void AChT_CharacterBase::OnEndRunning()
{
	WantsToRun = false;
}

/**
 * @brief Метод возвращающий идет ли персонаж
 * @return 
 */
bool AChT_CharacterBase::IsRunning() 
{
	return WantsToRun && IsMovingForward && !GetVelocity().IsZero();
}