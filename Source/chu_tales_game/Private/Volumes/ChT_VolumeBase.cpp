// chu_game, All Rights Reserved


#include "Volumes/ChT_VolumeBase.h"

// Sets default values
/**
 * @brief Конструктор
 */
AChT_VolumeBase::AChT_VolumeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
/**
 * @brief начало игры
 */
void AChT_VolumeBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
/**
 * @brief Обовление каждый кадр
 * @param DeltaTime дельта времени
 */
void AChT_VolumeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

