// chu_game, All Rights Reserved


#include "Components/ChT_WeaponComponent.h"

#include "GameFramework/Character.h"
#include "ChT_CharacterBase.h"

DEFINE_LOG_CATEGORY_STATIC(WeaponComponetLog, All, All);

/**
 * @brief
 * Конструктор класса
 */
UChT_WeaponComponent::UChT_WeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

/**
 * @brief Начало игры
 */
void UChT_WeaponComponent::BeginPlay()
{
	Super::BeginPlay();
	CharacterOwner = Cast<ACharacter>(GetOwner());
	SpawnWeapon();
	EquipWeapon();
}

/**
 * @brief Создать оружие в мире
 */
void UChT_WeaponComponent::SpawnWeapon()
{
	if (!GetWorld()) return;
	CurrentWeapon = GetWorld()->SpawnActor<AChT_SwordBase>(WeaponClass);
	if (!CurrentWeapon)return;
}

/**
 * @brief 
 * @param ToInputSocket Название сокета к которому прикрепить оружие
 */
void UChT_WeaponComponent::SwapWeaponSocket(FName ToInputSocket)
{
	if (!CurrentWeapon)return;
	FDetachmentTransformRules DetachmentRule(EDetachmentRule::KeepWorld, false);
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);

	CurrentWeapon->SetOwner(CharacterOwner);

	if (CurrentWeapon->IsAttachedTo(CharacterOwner))
	{
		CurrentWeapon->DetachFromActor(DetachmentRule);
	}
	CurrentWeapon->AttachToComponent(CharacterOwner->GetMesh(), AttachmentRules, ToInputSocket);
}

/**
 * @brief Экипировать оружие
 */
void UChT_WeaponComponent::EquipWeapon()
{
	UE_LOG(WeaponComponetLog, Display, TEXT("Equipped"));
	bIsEquipped = true;
	SwapWeaponSocket(HandSocketName);
}

/**
 * @brief Переместить оружие за спину
 */
void UChT_WeaponComponent::DeEquipWeapon()
{
	UE_LOG(WeaponComponetLog, Display, TEXT("Destroy"));

	bIsEquipped = false;
	SwapWeaponSocket(BackSocketName);
}

/**
 * @brief Уничтожить оружие
 */
void UChT_WeaponComponent::DestroyWeapon()
{
	if (!CurrentWeapon)return;
	CurrentWeapon->Destroy();
}

UAnimMontage* UChT_WeaponComponent::GetComboAnimation()
{
	if (ComboClass->ComboAnimations.Num() >= 0)
	{
		bCanStartNextCombo = true;
		UE_LOG(WeaponComponetLog, Display, TEXT("Combo cooldown start"),);
		GetWorld()->GetTimerManager().SetTimer(ComboTimerHandler, [&]()
		{
			UE_LOG(WeaponComponetLog, Display, TEXT("Combo cooldown end"),);
			ComboIndex = 0;
			// bCanStartNextCombo = false;
		}, ComboClass->ComboDelay, false, ComboClass->ComboDelay);

		if (bCanStartNextCombo)
		{
			if (ComboIndex < ComboClass->ComboAnimations.Num())
			{
				return ComboClass->ComboAnimations[ComboIndex++];
			}else
			{
				ComboIndex = 0;
			}
		}
		else
		{
			if (ComboClass->ComboAnimations.IsValidIndex(ComboIndex))
			{
				return ComboClass->ComboAnimations[ComboIndex];
			}
			
		}
	}
	return nullptr;
}

/**
 * @brief Атаковать
 */
void UChT_WeaponComponent::Attack()
{
	if (!FMath::IsNearlyZero(CurrentCoolDown))return;

	Cast<AChT_CharacterBase>(CharacterOwner)->bIsUpperBody = true;

	if (!bIsEquipped) EquipWeapon(); //Get weapon in hand

	float Delay = PlayAttackAnim();

	UE_LOG(WeaponComponetLog, Display, TEXT("Delay %f"), Delay);

	GetWorld()->GetTimerManager().SetTimer(AnimTimerHandler, this, &UChT_WeaponComponent::OnSwingEnd, Delay,
	                                       false, Delay);
	CurrentCoolDown = CurrentWeapon->CoolDown;
	GetWorld()->GetTimerManager().SetTimer(CoolDownTimerHandler, this, &UChT_WeaponComponent::OnCoolDownEnd,
	                                       CurrentCoolDown,
	                                       false, CurrentCoolDown);
	GetWorld()->GetTimerManager().SetTimer(DeEquipTimerHandler, this, &UChT_WeaponComponent::OnDeEquipTimer,
	                                       TimeToDeEquip,
	                                       false, TimeToDeEquip);
}

/**
 * @brief Сыграть анимацию атаки
 * @return Время выполнения анимации
 */
float UChT_WeaponComponent::PlayAttackAnim()
{
	const float InternalDelay = CharacterOwner->PlayAnimMontage(GetComboAnimation(), 1, NAME_None);
	CurrentWeapon->CanDealDamage = true;
	return InternalDelay;
}

/**
 * @brief Эвент по окончании анимации аитаки
 */
void UChT_WeaponComponent::OnSwingEnd()
{
	UE_LOG(WeaponComponetLog, Display, TEXT("Swing end!"));
	if (!CurrentWeapon)return;
	Cast<AChT_CharacterBase>(CharacterOwner)->bIsUpperBody = false;
	CurrentWeapon->CanDealDamage = false;
	GetWorld()->GetTimerManager().ClearTimer(AnimTimerHandler);;
}

/**
 * @brief Эвент по окончанию кулдауна
 */
void UChT_WeaponComponent::OnCoolDownEnd()
{
	CurrentCoolDown = 0.0f;
}

/**
 * @brief Таймер до перемещения 
 */
void UChT_WeaponComponent::OnDeEquipTimer()
{
	UE_LOG(WeaponComponetLog, Display, TEXT("DeEquip!"));
	DeEquipWeapon();
}
